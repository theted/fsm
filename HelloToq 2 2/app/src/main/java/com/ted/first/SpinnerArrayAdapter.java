package com.ted.first;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.util.Log;

/**
 * Created by k on 10/2/14.
 */
public class SpinnerArrayAdapter extends ArrayAdapter<String> {
    Context c;
    String[] s;
    public SpinnerArrayAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        c = context;
        s = (String[])objects;
    }
    @Override
    public View getView(int pos, View cv, ViewGroup p){
        return stuffSpinnerRow(pos,cv,p);
    }
    @Override
    public View getDropDownView(int pos, View cv, ViewGroup p){
        return stuffSpinnerRow(pos,cv,p);
    }

    public View stuffSpinnerRow(int pos, View cv, ViewGroup p) {
        if (cv == null) {
            LayoutInflater l = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cv = l.inflate(R.layout.spinner_row,p,false);
        }
        TextView tv = (TextView) cv.findViewById(R.id.textView1);
        tv.setText(s[pos]);
        ImageView iv = (ImageView)cv.findViewById(R.id.imageView1);
        if (s[pos] == "red"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.red);
        }
        if (s[pos] == "yellow"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.yellow);
        }
        if (s[pos] == "blue"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.blue);
        }
        if (s[pos] == "black"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.black);
        }

        if (s[pos] == "thin"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.circle_tiny);
        }
        if (s[pos] == "medium"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.circle_tiny1);
        }
        if (s[pos] == "thick"){
            Log.d("Color:", "reddddd!");
            iv.setImageResource(R.drawable.circle_tiny2);
        }

        return cv;
    }
}
