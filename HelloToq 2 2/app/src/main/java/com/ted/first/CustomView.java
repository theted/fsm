package com.ted.first;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.graphics.Path;
import android.graphics.Paint;
import android.graphics.Point;
import java.util.ArrayList;

/**
 * Created by k on 9/20/14.
 */
public class CustomView extends View{
    boolean clear = false;
    Path path = new Path();
    Paint paint = new Paint();
    float cx = 0;
    float cy = 0;
    float radius = 2f;
    ArrayList<Point> points = new ArrayList<Point>();
    ArrayList<Path> paths = new ArrayList<Path>();
    ArrayList<Paint> paints = new ArrayList<Paint>();
    //boolean quickClick = false;
    public void setPath(Path path) {
        this.path = path;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    public CustomView(Context context) {
        super(context);
    }

    protected void onDraw(Canvas c){ //the ide made me change to protected
        c.drawPath(path,paint);
        for (int i=0; i< paths.size();i++) {
            paint = paints.get(i);
            Path p = paths.get(i);
            Point p1 = points.get(i);
            c.drawPath(p,paint);
            c.drawCircle(p1.x, p1.y, radius, paint);
            //Log.d("pos: ", String.valueOf(paths.size()));
        }
        //if (quickClick){
        //c.drawPoint(cx,cy,paint);
        //for (Point p:points) {
            //c.drawCircle(p.x, p.y, radius, paint);
        //}
        //}
        if (clear){
            c.drawColor(Color.WHITE);
        }
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        super.setOnTouchListener(l);
    }

    public void undo(){
        if (paths.size() > 0) {
            paths.remove(paths.size() - 1);
            paints.remove(paints.size() - 1);
            points.remove(points.size() - 1);
        } //else add a toast?
    }

    public void clear() {
        clear = true;
    }
}
