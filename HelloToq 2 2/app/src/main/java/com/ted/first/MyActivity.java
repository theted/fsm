package com.ted.first;

import android.annotation.SuppressLint;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;

import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.graphics.Point;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.util.Log;
import android.graphics.Bitmap;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import android.graphics.Bitmap.CompressFormat;
import android.content.Context;
import android.widget.ToggleButton;
import android.graphics.Color;

import java.net.URI;
import java.util.UUID;
import java.util.ArrayList;
import java.util.Random;
import android.provider.MediaStore;
import android.app.Activity;

import static android.widget.AdapterView.OnItemSelectedListener;

import com.gmail.yuyang226.flickrj.sample.android.FlickrHelper;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import com.gmail.yuyang226.flickrj.sample.android.FlickrjActivity;

import javax.xml.parsers.ParserConfigurationException;

public class MyActivity extends Activity {

    Bitmap prize;
    Boolean complete = false;

    final Paint paint = new Paint();
    int storedColor = Color.BLACK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final int[] colorss = {Color.RED,Color.BLACK,Color.YELLOW,Color.BLUE};
        final Random r = new Random();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        ViewGroup addTo = (ViewGroup) findViewById(R.id.addTo);

        Spinner s = (Spinner)findViewById(R.id.spinnercolor);
        String[] stuff = {"black","blue","yellow","red","random!"};
        s.setAdapter(new SpinnerArrayAdapter(this,R.layout.spinner_row,stuff));



        //final Paint paint = new Paint();




            Spinner s1 = (Spinner) findViewById(R.id.spinnerbrush);
            String[] stuff1 = {"thin", "medium", "thick"};
            s1.setAdapter(new SpinnerArrayAdapter(this,R.layout.spinner_row, stuff1));

            final CustomView v = new CustomView(this);
            //v.setDrawingCacheEnabled(true);
            //v.setBackgroundColor(Color.GREEN); So by doing this test, I proved that the custom view matches/fills parent--that is the whole screen

            final Path p = new Path();
            final ArrayList<Path> paths = new ArrayList<Path>();
            final ArrayList<Paint> paints = new ArrayList<Paint>();
            //final Paint paint = new Paint();
            paint.setStrokeWidth(10);
            paint.setStyle(Paint.Style.STROKE);//This did the trick!
            v.setPaint(paint);
            View.OnTouchListener toAttach = new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //view.setBackgroundColor(Color.GREEN);//and success.  Upon touching, the background changes again..so successful attachment of the listener!

                    //stackoverflow question 4300399 ("android drawing a line to follow your finger")


                    int coordX = (int) motionEvent.getX();
                    int coordY = (int) motionEvent.getY();  //now extracted where finger's at!
                    //Path p;
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            //p = new Path();

                            //v.quickClick = false;
                            p.moveTo(coordX, coordY); //this forces me to make p a final variable.
                            //paths.add(p);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (p != null) {
                                p.lineTo(coordX, coordY);
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            //Point p = new Point();
                            Point p1 = new Point();
                            p1.x = coordX;
                            p1.y = coordY;

                            //v.paths.add(p);
                            Paint ppp = new Paint(paint);
                            Path pp = new Path(p);
                            v.paths.add(pp);
                            v.points.add(p1);
                            v.paints.add(ppp);
                            p.reset();
                            break;
                        //v.quickClick = true;
                        default:
                            return false;

                    }
                    //this feels so WRONG.  want to modify the passed in "view" but need to access setPath and can't really cast to customview in the signature....
                    v.setPath(p);
                    //v.paths = paths;

                    v.invalidate();
                    return true; //"return true if the listener has consumed the event--from docs. so I changed from false to true as this should be the case
                }
            };

            v.setOnTouchListener(toAttach);


            //setContentView(v);
            addTo.addView(v);


        s.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                switch (pos) {
                    case 0:
                        paint.setColor(Color.BLACK);
                        storedColor = Color.BLACK;
                        break;
                    case  1:
                        paint.setColor(Color.BLUE);
                        storedColor = Color.BLUE;
                        break;
                    case  2:
                        paint.setColor(Color.YELLOW);
                        storedColor = Color.YELLOW;
                        break;
                    case  3:
                        paint.setColor(Color.RED);
                        storedColor = Color.RED;
                        break;
                    case 4:
                        //Color r = new Color();


                        int change = colorss[r.nextInt(colorss.length)];
                        paint.setColor(change);
                        break;
                        //v.undo();
                        //v.invalidate();
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button undo = (Button)findViewById(R.id.undobutton);
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.undo();
                v.invalidate();
            }
        });
        Button submit = (Button)findViewById(R.id.submit);
        //BitmapDrawable d;
        submit.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {


                                          //Flickr
                                          LinearLayout stuff = (LinearLayout) findViewById(R.id.addTo);
                                          View pls = stuff;
                                          pls.setDrawingCacheEnabled(true);
                                          Bitmap send = pls.getDrawingCache();
                                          ByteArrayOutputStream s = new ByteArrayOutputStream();
                                          send.compress(Bitmap.CompressFormat.PNG, 100, s);
                                          byte[] bytes = s.toByteArray();




                                          showImage();
                                          while (!complete){
                                              continue;
                                          }
                                          Bitmap altered = Bitmap.createScaledBitmap(prize,250,288,false);

                                            /*
                                          String testImgSaved = MediaStore.Images.Media.insertImage(
                                                  getContentResolver(), altered,
                                                  UUID.randomUUID().toString() + ".png", "drawing");
                                            */









                                          Intent back = new Intent(getApplicationContext(),MyFirstActivity.class);
                                          back.putExtra("dl", altered); //dl = downloaded-need to put on watch
                                          back.putExtra("ul",bytes); //ul = upload--need to put on flickr
                                          //setResult(RESULT_OK,back);
                                          startActivity(back);















                                           //Flickr
                                          /*
                                          LinearLayout stuff = (LinearLayout) findViewById(R.id.addTo);
                                          View pls = stuff;
                                          pls.setDrawingCacheEnabled(true);
                                          String imgSaved = MediaStore.Images.Media.insertImage(
                                                  getContentResolver(), pls.getDrawingCache(),
                                                  UUID.randomUUID().toString() + ".png", "drawing");
                                          pls.destroyDrawingCache();
                                          Log.d("url", imgSaved);
                                          Uri imageUri = Uri.parse(imgSaved);
                                          Log.d("uri", imageUri.toString());

                                          String selectedImagePath = getPath(imageUri);
                                          File fileUri = new File(selectedImagePath);


                                          Intent intent = new Intent(getApplicationContext(),
                                                  FlickrjActivity.class);
                                          intent.putExtra("flickImagePath", fileUri.getAbsolutePath());

                                          startActivity(intent);

                                          */


                                      }
                                  }


        );





        s1.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                switch (pos) {
                    case 0:
                        paint.setStrokeWidth(5);
                        break;
                    case  1:
                        paint.setStrokeWidth(10);
                        break;
                    case  2:
                        paint.setStrokeWidth(15);
                        break;

                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        }

/*
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            Intent back = new Intent(getApplicationContext(),MyFirstActivity.class);
            back.putExtra("bitmap", prize);
            startActivity(back);
        }
    }
    */

    private void showImage() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    String svr="www.flickr.com";

                    REST rest=new REST();
                    rest.setHost(svr);

                    //initialize Flickr object with key and rest
                    Flickr flickr=new Flickr(FlickrHelper.API_KEY,rest);

                    //initialize SearchParameter object, this object stores the search keyword
                    SearchParameters searchParams=new SearchParameters();
                    searchParams.setSort(SearchParameters.INTERESTINGNESS_DESC);

                    //Create tag keyword array
                    String[] tags=new String[]{"cs160fsm"};
                    searchParams.setTags(tags);

                    //Initialize PhotosInterface object
                    PhotosInterface photosInterface=flickr.getPhotosInterface();
                    //Execute search with entered tags
                    PhotoList photoList=photosInterface.search(searchParams,20,1);

                    //get search result and fetch the photo object and get small square imag's url
                    if(photoList!=null){
                        //Get search result and check the size of photo result
                        Random random = new Random();
                        int seed = random.nextInt(photoList.size());
                        //get photo object
                        Photo photo=(Photo)photoList.get(seed);

                        //Get small square url photo
                        InputStream is = photo.getMediumAsStream();
                        prize = BitmapFactory.decodeStream(is);
                        complete = true;
                        /*
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ImageView imageView = (ImageView) findViewById(R.id.imview);
                                imageView.setImageBitmap(bm);
                            }
                        });
                        */
                    }
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (FlickrException e) {
                    e.printStackTrace();
                } catch (IOException e ) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }


        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void toggleEraser(View view) {
        boolean selected = ((ToggleButton) view).isChecked();
        if (selected) {
            paint.setColor(Color.WHITE);
        } else {
            paint.setColor(storedColor);
        }
    }
}
