package com.ted.first;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Debug;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.util.Log;
import android.provider.MediaStore;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import android.net.Uri;
import com.gmail.yuyang226.flickrj.sample.android.FlickrHelper;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import com.gmail.yuyang226.flickrj.sample.android.FlickrjActivity;

import javax.xml.parsers.ParserConfigurationException;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;

import java.io.InputStream;

//import edu.berkeley.cs160.andrewbfang.hellotoq.R;


public class MyFirstActivity extends Activity {

    boolean first = true;
    int test = 0;

    private DeckOfCardsEventListener deckOfCardsEventListener;  //new

    // Handle card events triggered by the user interacting with a card in the installed deck of cards
    private class DeckOfCardsEventListenerImpl implements DeckOfCardsEventListener{

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardOpen(String)
         */
        public void onCardOpen(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    //Toast.makeText(MyFirstActivity.this, "cardIDasd: " + cardId, Toast.LENGTH_SHORT).show();
                    launchDrawActivity();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardVisible(String)
         */
        public void onCardVisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    //Toast.makeText(ToqApiDemo.this, getString(R.string.event_card_visible) + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardInvisible(String)
         */
        public void onCardInvisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    //Toast.makeText(ToqApiDemo.this, getString(R.string.event_card_invisible) + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardClosed(String)
         */
        public void onCardClosed(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    //Toast.makeText(ToqApiDemo.this, getString(R.string.event_card_closed) + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(String, String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption){
            runOnUiThread(new Runnable(){
                public void run(){
                    //Toast.makeText(ToqApiDemo.this, getString(R.string.event_menu_option_selected) + cardId + " [" + menuOption + "]", Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(String, String, String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption, final String quickReplyOption){
            runOnUiThread(new Runnable(){
                public void run(){
                    //Toast.makeText(ToqApiDemo.this, getString(R.string.event_menu_option_selected) + cardId + " [" + menuOption + ":" + quickReplyOption +
                            //"]", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


    LocationManager locationManager;
    LocationListener locationListener;



    public DeckOfCardsManager mDeckOfCardsManager;
    public RemoteDeckOfCards mRemoteDeckOfCards;
    public RemoteResourceStore mRemoteResourceStore;

    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    private void sendNotification() {
        String[] message = new String[2];
        message[0] = "Congratulations";
        message[1] = "You've earned a prize";
        // Create a NotificationTextCard
        NotificationTextCard notificationCard = new NotificationTextCard(System.currentTimeMillis(),
                "Thanks for submitting!", message);

        // Draw divider between lines of text
        notificationCard.setShowDivider(true);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);

        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification(notification);
            //Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to send Notification", Toast.LENGTH_SHORT).show();
        }
    }

    //StackOverflow Question id 363681
    public static int randomInt(int min, int max){
        Random rand = new Random();
        return rand.nextInt((max-min)) + min;
    }

    private void sendInitialNotification(int i) {
        String[] message = new String[2];
        message[0] = "special info";
        message[1] = "Draw Text";
        String name = "name";
        //int rand = randomInt(1,6);
        //Wikepedia for the info on notifications!
        switch (i) {
            case 1:
                message[0] = "1942-1996";
                message[1] = "Express your view of free speech";
                name = "Mario Savio";
                break;
            case 2:
                message[0] = "Don't trust anyone over 30";
                message[1] = "Draw Text: FSM";
                name = "Jack Weinberg";
                break;
            case 3:
                message[0] = "Born 1941";
                message[1] = "Draw a Megaphone";
                name = "Joan Baez";
                break;
            case 4:
                message[0] = "Born: 1944";
                message[1] = "Draw Text: SLATE";
                name = "Jackie Goldberg";
                break;
            case 5:
                message[0] = "1939-2008";
                message[1] = "Draw Text: Free Speech";
                name = "Michael Rossmann";
                break;
            case 6:
                message[0] = "Born 1942";
                message[1] = "Draw Text: Now";
                name = "Art Goldberg";
                break;
            default:
                message[0] = "special info";
                message[1] = "Draw Text";
                name = "name";
                break;



        }


        // Create a NotificationTextCard





        NotificationTextCard notificationCard = new NotificationTextCard(System.currentTimeMillis(),
                name, message);

        // Draw divider between lines of text
        notificationCard.setShowDivider(true);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);




        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification(notification);
            //Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to send Notification", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){

        Log.d("Saved", "hopefully");
        savedInstanceState.putInt("return", 1);
        super.onSaveInstanceState(savedInstanceState);
    }
    */


    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_first);

        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());


        deckOfCardsEventListener= new DeckOfCardsEventListenerImpl(); //new

        mDeckOfCardsManager.addDeckOfCardsEventListener(deckOfCardsEventListener);

        init();

        try {
            Intent t = getIntent();

            //Upload to Flikr
            byte[] bytes = getIntent().getByteArrayExtra("ul");
            Bitmap prize = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            String imgSaved1 = MediaStore.Images.Media.insertImage(
                    getContentResolver(), prize,
                    "plsshowup" + ".png", "drawing");
            Log.d("showuppls", imgSaved1);
            Uri imageUri = Uri.parse(imgSaved1);
            String selectedImagePath = getPath(imageUri);
            File fileUri = new File(selectedImagePath);


            Intent intent = new Intent(getApplicationContext(),
                    FlickrjActivity.class);
            intent.putExtra("flickImagePath", fileUri.getAbsolutePath());

            startActivity(intent);
            //photo successfully uploaded


            //Final part :(

            Bitmap prize1 = (Bitmap)getIntent().getParcelableExtra("dl");

            //BitmapDrawable bd = new BitmapDrawable(prize1);
            //findViewById(R.id.root).setBackground(bd);
            //addSimpleTextCard();
            updateDeckOfCardsFromUI();
            CardImage ci = new CardImage("ci", prize1);
            mRemoteResourceStore.addResource(ci);
            ListCard listCard= mRemoteDeckOfCards.getListCard();
            SimpleTextCard append = (SimpleTextCard)listCard.childAtIndex(6);
            String[] plea = {"Your Prize!"};
            append.setMessageText(plea);
            append.setCardImage(mRemoteResourceStore,ci);


            try{
                mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
                Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();
            }
            sendNotification();
            Button b = (Button)findViewById(R.id.btn_hello);
            b.setText("Thanks for drawing!");


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            /*
            CardImage ci = new CardImage("ci", newi);
            mRemoteResourceStore.addResource(ci);
            ListCard lc = mRemoteDeckOfCards.getListCard();
            SimpleTextCard toalter = (SimpleTextCard)lc.childAtIndex(6);
            toalter.setCardImage(mRemoteResourceStore,ci);
            lc.add(toalter);
            try {
                mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards,mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
            }

            */


            //Bitmap append = getIntent().getParcelableExtra("dl");
            //Log.d("nevergoing", "kg");

            //Bitmap altered = Bitmap.createScaledBitmap(toadd,250,288,false);

            /*

            //CardImage toadd = new CardImage("next",altered);

            //mRemoteResourceStore.addResource(toadd);

            ListCard listCard = mRemoteDeckOfCards.getListCard();
            Log.d("not see this", "sdf");
            /*
            Log.d("Size of listCards", Integer.toString(listCard.size()) );
            SimpleTextCard toaddto = (SimpleTextCard)listCard.childAtIndex(6);
            //toaddto.setCardImage(mRemoteResourceStore,toadd);
            String[]testmessage = {"test pls work"};
            toaddto.setMessageText(testmessage);
            try{
                mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards,mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e){
                Log.d("sorry", "much");
            }
            */
            // Acquire a reference to the system Location Manager
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            // Define a listener that responds to location updates
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    //Try for <50 location stuff
                    Location dest = new Location("dest");
                    Location curr = new Location("curr");

                    //MAKE SPROUL THE DEST
                    dest.setLatitude(37.86965);
                    dest.setLongitude(-122.25914);

                    /*
                    //DELETE NEXT TWO LINES AS THESE ARE FOR MY HOUSE IN FREMONT--NOT SPROUL
                    dest.setLatitude(37.5089863);
                    dest.setLongitude(-121.9183948);
                    */

                    curr.setLatitude(latitude);
                    curr.setLongitude(longitude);
                    int radius = 10; //SET THIS TO 10 IN FINAL SUBMIT CODE!!!!!!!!!
                    if (curr.distanceTo(dest)<radius) {
                        test = randomInt(1,6);

                        Log.d("testttttttt", Integer.toString(test));
                        sendInitialNotification(test);
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {}

                public void onProviderEnabled(String provider) {}

                public void onProviderDisabled(String provider) {}
            };

            // Register the listener with the Location Manager to receive location updates
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                // Get update every 5 seconds
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 0, locationListener);
            }

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // Get update every 5 seconds
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100000, 0, locationListener);
            }


        } catch (NullPointerException e) {
            Log.d("hExceptionnnn", "Caught");
        }









        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();


                //Try for <50 location stuff
                Location dest = new Location("dest");
                Location curr = new Location("curr");

                dest.setLatitude(37.86965);
                dest.setLongitude(-122.25914);

                //DELETE NEXT TWO LINES AS THESE ARE FOR MY HOUSE IN FREMONT--NOT SPROUL
                /*
                dest.setLatitude(37.5089863);
                dest.setLongitude(-121.9183948);
                */
                curr.setLatitude(latitude);
                curr.setLongitude(longitude);
                int radius = 10; //SET THIS TO 10 IN FINAL SUBMIT CODE!!!!!!!!!
                if (curr.distanceTo(dest)<radius) {
                    test = randomInt(1,6);

                    Log.d("testttttttt", Integer.toString(test));
                    sendInitialNotification(test);
                }

                //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_SHORT).show();
                /*
                if (latitude <38) {  //REPLACE WITH SPROUL PLAZA CHECKING CODE
                    test = randomInt(1,6);

                    Log.d("testttt", Integer.toString(test));
                    sendInitialNotification(test);
                }
                */
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100000, 0, locationListener);
        }

































        findViewById(R.id.btn_hello).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                install();
            }
        });
        //sendNotification();
        //install();
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void install() {
        updateDeckOfCardsFromUI();
        try{
            mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
        }
        catch (RemoteDeckOfCardsException e){
            e.printStackTrace();
            Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();
        }

    }


    private void updateDeckOfCardsFromUI() {

        if (mRemoteDeckOfCards == null) {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        ListCard listCard= mRemoteDeckOfCards.getListCard();
        // Card #1
        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(0);
        simpleTextCard.setHeaderText("Mario Savio");
        //simpleTextCard.setTitleText("World world world");
        String[] messages = {"Express Your Own View by Drawing!"};
        simpleTextCard.setMessageText(messages);
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setShowDivider(true);


        CardImage[] mCardImages = new CardImage[6];
        try {
            mCardImages[0] = new CardImage("card.image.0", getBitmap("mario_savio_toq.png"));
            mCardImages[1] = new CardImage("card.image.1", getBitmap("art_goldberg_toq.png"));
            mCardImages[2] = new CardImage("card.image.2", getBitmap("jackie_goldberg_toq.png"));
            mCardImages[3] = new CardImage("card.image.3", getBitmap("jack_weinberg_toq.png"));
            mCardImages[4] = new CardImage("card.image.4", getBitmap("joan_baez_toq.png"));
            mCardImages[5] = new CardImage("card.image.5", getBitmap("michael_rossman_toq.png"));
        } catch (Exception e){};
        simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[0]);

        simpleTextCard.setReceivingEvents(true);

        //card2
        SimpleTextCard second = (SimpleTextCard)listCard.childAtIndex(1);//new
        second.setHeaderText("Art Goldberg");//new
        second.setCardImage(mRemoteResourceStore,mCardImages[1]);
        String[] messages1 = {"Draw 'Now'! "};
        second.setMessageText(messages1);
        second.setReceivingEvents(true);

        //card3
        SimpleTextCard third = (SimpleTextCard)listCard.childAtIndex(2);//new
        third.setHeaderText("Jackie Goldberg");//new
        third.setCardImage(mRemoteResourceStore,mCardImages[2]);
        String[] messages2 = {"Draw 'SLATE'! "};
        third.setMessageText(messages2);
        third.setReceivingEvents(true);
        third.setShowDivider(true);

        //card4
        SimpleTextCard fourth = (SimpleTextCard)listCard.childAtIndex(3);//new
        fourth.setHeaderText("Jack Weinberg");//new
        fourth.setCardImage(mRemoteResourceStore,mCardImages[3]);
        String[] messages3 = {"Draw 'FSM'! "};
        fourth.setMessageText(messages3);
        fourth.setReceivingEvents(true);
        fourth.setShowDivider(true);

        //card5
        SimpleTextCard fifth = (SimpleTextCard)listCard.childAtIndex(4);//new
        fifth.setHeaderText("Joan Baez");//new
        fifth.setCardImage(mRemoteResourceStore,mCardImages[4]);
        String[] messages4 = {"Draw a Megaphone! "};
        fifth.setMessageText(messages4);
        fifth.setReceivingEvents(true);
        fifth.setShowDivider(true);

        //card6
        SimpleTextCard sixth = (SimpleTextCard)listCard.childAtIndex(5);//new
        sixth.setHeaderText("Michael Rossman");//new
        sixth.setCardImage(mRemoteResourceStore,mCardImages[5]);
        String[] messages5 = {"Draw 'Free Speech'! "};
        sixth.setMessageText(messages5);
        sixth.setReceivingEvents(true);
        sixth.setShowDivider(true);

        //test for altering the 7th card


    }

//
    // Create some cards with example content
    private RemoteDeckOfCards createDeckOfCards(){

        ListCard listCard= new ListCard();
        SimpleTextCard simpleTextCard= new SimpleTextCard("card0");

        SimpleTextCard second = new SimpleTextCard("card1");//new

        SimpleTextCard third = new SimpleTextCard("card2");
        SimpleTextCard fourth = new SimpleTextCard("card3");
        SimpleTextCard fifth = new SimpleTextCard("card4");
        SimpleTextCard sixth = new SimpleTextCard("card5");
        SimpleTextCard append = new SimpleTextCard("append");

        listCard.add(simpleTextCard);

        listCard.add(second);//new
        listCard.add(third);
        listCard.add(fourth);
        listCard.add(fifth);
        listCard.add(sixth);
        listCard.add(append);
        return new RemoteDeckOfCards(this, listCard);
    }

//    Initialise
    private void init(){

        // Create the resourse store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();
        // Try to retrieve a stored deck of cards
        try {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        catch (Throwable th){
            th.printStackTrace();
        }
    }

    /**
     * @see android.app.Activity#onStart()
     */
    protected void onStart(){
        super.onStart();

        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
    }

    private void launchDrawActivity() {
        Intent i = new Intent(MyFirstActivity.this,MyActivity.class);
        first = false;
        //startActivityForResult(i,666);
        startActivity(i);
        locationManager.removeUpdates(locationListener);
    }

    /*protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 666) {
            Bitmap prize = data.getParcelableExtra("dl");
            String imgSaved1 = MediaStore.Images.Media.insertImage(
                    getContentResolver(), prize,
                    "plsshowup" + ".png", "drawing");
            Log.d("showuppls",imgSaved1);
        }
    }*/

    private void addSimpleTextCard() {

        ListCard listCard = mRemoteDeckOfCards.getListCard();
        Log.d("dbzcalled", "nao");
        int currSize = listCard.size();

        // Create a SimpleTextCard with 1 + the current number of SimpleTextCards
        SimpleTextCard simpleTextCard = new SimpleTextCard(Integer.toString(currSize+1));

        simpleTextCard.setHeaderText("Header: " + Integer.toString(currSize+1));
        simpleTextCard.setTitleText("Title: " + Integer.toString(currSize+1));
        String[] messages = {"Message: " + Integer.toString(currSize+1)};
        simpleTextCard.setMessageText(messages);
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setShowDivider(true);

        listCard.add(simpleTextCard);

        try {
            mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards,mRemoteResourceStore);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to Create SimpleTextCard", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
